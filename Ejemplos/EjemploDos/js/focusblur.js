//Elemento con la capa de ayuda a mosytat en los campos de formulario
var txtoAyuda;
//Matriz con los mensajes de ayuda asociados a cada control de formulario
var arregloAyuda =[
    "Ingrese su nombre en este campo de texto",
    "Ingrese su Apellido en este campo de texto",
    "Ingrese su direccion de correo en formato usurio@domino.com",
    "Ingrese su número telefonico en el formato 99990000",
    "Ingrese una descripcion breve en el campo área de texto",
    "Seleccione su profecion en el campo select",
    "Diganos cuál es su país de origen con este campo select",
    "Reestablezca el formulario con este boton",
    "Envie el formulario con este boton",
    ""
];

//Inicializamos el elemento tetoAyudaDiv y registrar los manejadores de eventos para los distintos controles del formulario

function inic() {
    textoAyuda = document.getElementById("textoAyuda");
    //registrar los escuchadores de Eventos
    registrarEscuchas(document.getElementById("firstname"), 0);
    registrarEscuchas(document.getElementById("lastname"),  1);
    registrarEscuchas(document.getElementById("email"),     2);
    registrarEscuchas(document.getElementById("phone"),     3);
    registrarEscuchas(document.getElementById("describe"),  4);
    registrarEscuchas(document.getElementById("profesion"), 5);
    registrarEscuchas(document.getElementById("selpais"),   6);
    registrarEscuchas(document.getElementById("resetbtn"),  7);
    registrarEscuchas(document.getElementById("submitbtn"), 8);
}

//Funcion que determina que mensaje de ayuda habilitar con base en al numeroMensaje enviado como segundo argumento
function registrarEscuchas(objeto, numeroMensaje) {
    //Asocual el manejador de eventos onfocus dependiendo del objeto y numeroMensaje recibido como argumentos

    objeto.addEventListener("focus", function() {
        textoAyuda.style.visibility = "visible";
        textoAyuda.innerHTML = arregloAyuda[numeroMensaje];
    }, false);
    objeto.addEventListener("blur", function() {
        textoAyuda.style.visibility = "hidden";
        textoAyuda.innerHTML = arregloAyuda[9];
    }, false);

}

//Desencadenando la guncion inic al cargar el documento
window.addEventListener("load",inic,false);
