var valor,STotal,SNumeros,Promedio; 
var suma1 = 0;
var suma2 = 0;
var suma3 = 0;
var contador1 = 0;
var contador2 = 0;
var contador3 = 0;
var s1 = document.getElementById("s1");
var s2 = document.getElementById("s2");
var s3 = document.getElementById("s3");
var s4 = document.getElementById("s4");
var s5 = document.getElementById("s5");
var s6 = document.getElementById("s6");
var variable = document.getElementById("valConversion");
var text = document.getElementById("text");
var cajaText = document.getElementById("valor");
var boton = document.getElementById("boton");
const cantidades = [];
function pulsar(e) {
	text.style.cssText = 'color: #EECA00';
	if (e.keyCode === 13) {
		e.preventDefault();
		if (document.formConver.valor.value.length == 0) {
			text.innerHTML = "¡Tiene que escribir una cantidad!";
	      	document.formConver.valor.focus();
	      	return 0;
	    }else{
	    	if (isNaN(formConver.valor.value)==true){
	    		text.innerHTML = "¡Ingrese un valor numérico!";
			}else if (formConver.valor.value < 0){
				text.innerHTML = "¡Ingrese un valor positivo!";
			}else if(formConver.valor.value > 200){
				text.innerHTML = "¡Excede el valor maxino, ingrese uno menor!";
				valor = Number(document.getElementById("valor").value);
				cantidades.push(valor);
				document.getElementById("formConver").reset();
			}
			else{
				valor = Number(document.getElementById("valor").value);
				cantidades.push(valor);
				document.getElementById("formConver").reset();
				document.formConver.valor.focus();
				variable.innerHTML = " ";
				text.innerHTML = " ";
				for (var i = 0; i <= cantidades.length -1; i++) {
					if (cantidades[i] >= 0 && cantidades[i] <= 200) {
						var parrafo = document.createElement("li");
						var contenido = document.createTextNode("$"+cantidades[i]);
						parrafo.appendChild(contenido);
						variable.appendChild(parrafo);
						console.log(valor);
					}
				}
			}
	    }
	}  
}
function calcular(){
	text.innerHTML = " ";
	for (var i = 0; i <= cantidades.length -1; i++) {
		if (cantidades[i] <= 70) {
			contador1++;
			suma1 += cantidades[i];
			console.log (cantidades[i]);
		}else if(cantidades[i] > 70 && cantidades[i] <= 200){
			contador2++;
			suma2 += cantidades[i];
		}else if (cantidades[i] > 200){
			contador3++;
			suma3 += cantidades[i];
		}
	}
	STotal = suma1 + suma2;
	SNumeros = contador1 + contador2;
	Promedio = STotal / SNumeros;
	s1.innerHTML = "Numero de ventas menores de $70: "+ contador1+ ", con un total de: $"+ (suma1).toFixed(2);
	s2.innerHTML = "Numero de ventas mayores de $70 y menores o iguales que $200 : "+ contador2+ ", con un total de: $"+ (suma2).toFixed(2);
	s3.innerHTML = "Numero de ventas mayores de $200: "+ contador3+ ", con un total de: $"+ (suma3).toFixed(2);
	s4.innerHTML = "Total de todas las ventas: $" + STotal.toFixed(2);
	s5.innerHTML = "Promedio total de las ventas: $" + Promedio.toFixed(2);
	boton.disabled = true;
	cajaText.disabled = true;
	if (boton.disabled == true) {
		cajaText.style.cssText = 'color: #858080';
		boton.style.cssText = 'background-color: #858080; border: 4px solid #858080';
	}
}
function carga(){
	document.getElementById("formConver").reset();
}