var btn;
var res;
function num() {
    return document.getElementById("num").value;
}
function factorialIterativo(n) {
    let s = 1;
    while (n > 1) {
        s *= n;
        n--;
    }
    return s;
}
function init() {
    btn = document.getElementById("evaluar");
    res = document.getElementById("resul");
    if (btn.addEventListener) {       
        btn.addEventListener("click", function(evt){
            var resultado = factorialIterativo(num());
            console.log(resultado);
            res.innerHTML= "El factorial de " + num() + " es: " + resultado;
        }, false);
    }
}
window.onload = init;